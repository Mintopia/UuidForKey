<?php

namespace Mintopia\UuidForKey;

use Ramsey\Uuid\Uuid;

trait UuidForKey
{
	public static function bootUuidForKey()
	{
		static::creating(function ($model) {
			$uuid = Uuid::uuid4();
			$model->incrementing = false;
			$model->{$model->getKeyName()} = $uuid->toString();
		});
	}
}